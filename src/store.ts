import { createStore , applyMiddleware} from 'redux'
import GetDataReducer from './Reducer/GetDataReudcer'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunk from 'redux-thunk'

const Store = createStore(GetDataReducer, composeWithDevTools(applyMiddleware(thunk)))
export type RootStore = ReturnType<typeof GetDataReducer>

export default Store