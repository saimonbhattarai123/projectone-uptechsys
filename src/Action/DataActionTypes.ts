export const DATA_LAODING = "DATA_LOADING"
export const DATA_FAIL = "DATA_FAIL"
export const DATA_SUCCESS = "DATA_SUCCESS"


export type DataType = {
    userId:number
    id:number
    title:string
    body: string
}

export type Data = {
    userId:number
    id:number
    title:string
    body: string
}

export interface DataLaoding {
    type: typeof DATA_LAODING
}

export interface DataFail{
    type: typeof DATA_FAIL
}

export interface DataSuccess{
    type: typeof DATA_SUCCESS
    payload: DataType
}

export type DataDispatchTypes = DataLaoding | DataFail | DataSuccess