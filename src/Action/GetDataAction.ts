import axios from 'axios'
import { Dispatch } from 'redux'
import { DATA_FAIL, DATA_SUCCESS } from './DataActionTypes'

export const getData = () => async (dispatch:Dispatch<any>) => {
    try{
        dispatch({
            type: "DATA_LOADING"
        })

        const res = await axios.get(`https://jsonplaceholder.typicode.com/posts`)

        
        dispatch({
            type:DATA_SUCCESS,
            payload: res.data
        })

        
    }

    catch(error) {
        dispatch({
            type: DATA_FAIL
        })
    }
}