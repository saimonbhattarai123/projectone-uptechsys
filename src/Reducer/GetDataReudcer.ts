import { DataDispatchTypes } from "../Action/DataActionTypes";

interface IDefaultState {
    loading: boolean,
    data?: any
}

const defaultState: IDefaultState = {
    loading: false,
}

const GetDataReducer = (state: IDefaultState = defaultState , action: DataDispatchTypes): IDefaultState => {
    switch(action.type){
        case "DATA_FAIL":
            return {
                loading:false
            }
        case "DATA_LOADING":
            return{
                loading:true,
            }
        case "DATA_SUCCESS":
            return{
                loading:false,
                data: action.payload
            }
        default:
            return state
    }
        
}

export default GetDataReducer;