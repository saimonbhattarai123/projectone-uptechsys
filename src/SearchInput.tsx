import React, { useState } from 'react';
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import { useDispatch, useSelector } from 'react-redux';
import { RootStore } from './store';
import './Style/SearchInput.css'
import axios from 'axios';



import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import SimpleModal from './ModalComponent';


const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      '& > *': {
        margin: theme.spacing(1),
      },
    },
  }),
);


  

export default function Inputs() {
  const classes = useStyles();
  const [Searchtitle,setSearchTitle] = useState("")
  const [isModalOpen,setModalOpen] = useState(false)
  const [dataofID,setDataOfId] = useState<any>({})

  const dispatch = useDispatch()
  const dataxx = useSelector((state: RootStore) => state.data)


  const fetchDataByID = (id:any) => {
    try {
      axios.get(`https://jsonplaceholder.typicode.com/posts/${id}`)
      .then(res => {
        const persons = res.data;
        setDataOfId(persons)
      })
    
    } catch (error) {
      console.log(error)
    }
    
    setModalOpen(true)
  }

  return (
    <form className={classes.root} noValidate autoComplete="off">
      <Input defaultValue={Searchtitle} placeholder="Search" inputProps={{ 'aria-label': 'description' }} onChange={(event) =>{setSearchTitle(event.target.value)}} />
      <div>
        <TableContainer component={Paper}>
      <Table aria-label="simple table">
        <TableHead>
          <TableRow className="search__heading">
            <TableCell id="Headingitem">User_id</TableCell>
            <TableCell id="Headingitem" align="right">Id</TableCell>
            <TableCell id="Headingitem" align="right">Title</TableCell>
            <TableCell className="actionContainer" id="Headingitem" align="right">Action</TableCell>
          </TableRow>
        </TableHead>
        {dataxx?.filter((data:any) =>{
          if (Searchtitle === ""){
            return data
          }
          else if (data.title.toLowerCase().includes(Searchtitle.toLowerCase())){
            return data
          }
        }).map((data:any) => (
          <TableBody>
            <TableRow>
              <TableCell id="bodyItem" component="th" scope="row">{data.userId}</TableCell>
              <TableCell id="bodyItem" align="right">{data.id}</TableCell>
              <TableCell id="bodyItem" align="right">{data.title}</TableCell>
              {/* <TableCell align="right" onClick={() => fetchDataByID(data.id)}>view</TableCell> */}
              <TableCell className="actionContainer" align="right">
              <button type="button" onClick={() => fetchDataByID(data.id)}>View</button>
              </TableCell>
              
            </TableRow>
        </TableBody>
       ))}
      </Table>
    </TableContainer>
    <SimpleModal  datax={dataofID} isModalOpen={isModalOpen} />
        </div>
    </form>
  )
        }