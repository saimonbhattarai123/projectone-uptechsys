import React from 'react';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import {  useState } from 'react';
import axios from 'axios';
import { useDispatch, useSelector } from 'react-redux';
import { RootStore } from './store';
import VisibilityIcon from '@material-ui/icons/Visibility';



function rand() {
  return Math.round(Math.random() * 20) - 10;
}

function getModalStyle() {
  const top = 50 + rand();
  const left = 50 + rand();

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    paper: {
      position: 'absolute',
      width: 400,
      backgroundColor: theme.palette.background.paper,
      border: '2px solid #000',
      boxShadow: theme.shadows[5],
      padding: theme.spacing(2, 4, 3),
    },
  }),
);

export default function SimpleModal(props: any) {
  const classes = useStyles();
  // getModalStyle is not a pure function, we roll the style only on the first render
  const [modalStyle] = React.useState(getModalStyle);




  const [isModalOpen,setModalOpen] = useState(false)
  const [dataofID,setDataOfId] = useState<any>({})

  const dispatch = useDispatch()
  const datax = useSelector((state: RootStore) => state.data)


    const fetchDataByID = (id:any) => {
        try {
          axios.get(`https://jsonplaceholder.typicode.com/posts/${id}`)
          .then(res => {
            const persons = res.data;
            setDataOfId(persons)
          })
        
        } catch (error) {
          console.log(error)
        }
        
        setModalOpen(true)
        
      }


  const handleClose = () => {
    setModalOpen(false);
  };


  const mystyle = {
    color: "white",
    backgroundColor: "#d92626",
    padding: "auto",
    fontFamily: "Arial",
    fontSize:"18px",
    height:"40px",
    width:"120px",
    marginLeft:"-60px",
    cursor:"pointer",
    marginRight:"30px"
   
    
  };

 


  return (
    <div>
        

        

      <Modal
        open={props.isModalOpen}
        onClose={handleClose}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        <div style={modalStyle} className={classes.paper}>
      <h2 id="simple-modal-title">Id: {props.datax.id}</h2>
      <p id="simple-modal-description">User-Id: {props.datax.userId}</p>
      <p id="simple-modal-description">Title: {props.datax.title}</p>
      <p id="simple-modal-description">Body: {props.datax.body}</p>
    </div>
      </Modal>
    </div>
  );
}
