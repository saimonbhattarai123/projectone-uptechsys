import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getData } from './Action/GetDataAction';
import Modal from 'react-modal';
import './App.css';
import { RootStore } from './store';


import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import axios from 'axios';
import FreeSoloCreateOption from './SearchInput';
import SimpleModal from './ModalComponent';



function App() {

  const dispatch = useDispatch()
  const datax = useSelector((state: RootStore) => state.data)
  console.log(datax)

  const [Searchtitle,setSearchTitle] = useState("")
  const [isModalOpen,setModalOpen] = useState(false)
  const [dataofID,setDataOfId] = useState<any>({})


  useEffect(() => {
    dispatch(getData())
  }, [dispatch]);



  const useStyles = makeStyles({
    table: {
      minWidth: 650,
    },
  });
  
    

  // const fetchDataByID = (id:any) => {
  //   try {
  //     axios.get(`https://jsonplaceholder.typicode.com/posts/${id}`)
  //     .then(res => {
  //       const persons = res.data;
  //       console.log("fgh",persons)
  //       setDataOfId(persons)
  //     })
    
  //   } catch (error) {
  //     console.log(error)
  //   }
    
  //   setModalOpen(true)
    
  // }

  

  return (
    <div className="App">

        <FreeSoloCreateOption />
      
    </div>

    
  );
}

export default App;
